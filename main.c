
#include <stdio.h>

// number of power measurements
#define POWER_LENGTH 96
#define POWER_HOUR 24


int main()
{

    // variables for reading the measurements from file
    double power [POWER_LENGTH] =  {
             7.0,  7.3,  7.7,  8.0,  8.3,  8.3,  8.3,  8.3,  8.8,  9.2,
             9.7, 10.1, 10.8, 10.8, 10.8, 11.3, 11.7, 13.1, 13.6, 13.6,
            13.6, 15.7, 16.3, 17.8, 21.8, 25.3, 25.3, 30.6, 38.8, 39.2,
            44.3, 44.3, 51.2, 52.6, 58.1, 55.3, 50.2, 42.0, 38.8, 37.2,
            25.8, 26.1, 26.1, 26.1, 26.1, 26.1, 27.3, 26.1, 25.4, 22.3,
            20.9, 18.3, 18.3, 18.1, 18.1, 18.1, 17.5, 17.3, 19.3, 20.5,
            28.3, 28.3, 35.7, 44.2, 38.2, 32.1, 27.8, 26.3, 25.1, 25.1,
            25.1, 25.1, 22.3, 20.8, 18.7, 16.9, 16.9, 16.9, 15.8, 13.9,
            13.2, 12.8, 12.6, 12.3, 12.0, 10.8, 10.4, 12.0, 10.8,  9.8,
            9.2, 9.0, 7.8, 7.2, 7.1, 6.8
    };

    double powhour[POWER_HOUR] = {0.0};

    // variables for computing the minimum and maximun etc.
    double p, pmin, pmax, pmit;
    pmin = 100;

    int maxtime=0;

    double hoursum=0.0;
    int h = 0;

    // print program header
    printf("\n\nProgramm zur Analyse eines Lastprofils\n");
    printf("--------------------------------------------------------\n");

    // the following loop reads a new value in every iteration
    for ( int i=0; i<POWER_LENGTH; i++ )
    {
        p = power[i];
        pmit += power[i];
        printf("Gelesener Wert: %3d %02d:%02d %lf\n", i, i/4, 15*(i%4), p);

        hoursum += p;
        h++;

        if(h==4){
            powhour[i/4] = hoursum / 4;
            h=0;
            hoursum=0.0;
        }


        // compute minimum and maximum
        if (p > pmax) {
            pmax = p;
            maxtime = i;
        }

        if (p < pmin)
            pmin = p;
    }

    pmit = pmit / POWER_LENGTH;
    // Ausgabe
    printf("\nErgebnisse:\n");
    printf("-----------\n\n");
    printf("Hoechstlast  . . . . . . . . . . . . : %6.2lf\n",pmax);
    printf("Grundlast    . . . . . . . . . . . . : %6.2lf\n",pmin);
    printf("Mittlerelast . . . . . . . . . . . . : %6.2lf\n",pmit);

    printf("Max Index     . . . . . . . . . . . . : %d\n",maxtime);
    printf("Maximum Zeit    . . . . . . . . . . . : %02d:%02d\n",maxtime/4, 15*(maxtime%4));

    // print table of 24 mean values
    printf ("\n");
    for (int i=0;i<24;i++) {
        printf ("%02d %6.2lf\n", i, powhour[i]);

    }

}
