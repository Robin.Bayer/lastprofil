# Aufgabe: Lastprofil eines Kraftwerks

Eine Tagesbelastungskurve stellt die Belastung eines Kraftwerkes über einen Zeitraum von 24 Stunden dar. Aus der Tagesbelastungskurve kann die Elektrizitätswirtschaft viele für den Betrieb von Kraftwerken wichtige Informationen gewinnen.

Man erkennt z. B., dass ein bestimmter Leistungsanteil ständig vom Kraftwerk geliefert werden muss. Dieser Wert, der zu jeder Zeit mindestens bereitgestellt werden muss, bezeichnet man als Grundlast. Den oberen Teil der Tagesbelastungskurve bezeichnet man als Spitzenlast, die höchste, zu einem bestimmten Zeitpunkt tatsächlich auftretende Belastung ist die Höchstlast (maximale Leistung). In dieser Aufgabe soll ein Programm entwickelt werden, das diese Kennzahlen ermittelt: die mittlere Leistung (arithmetischer Mittelwert), die Grundlast (Minimum) und die Höchstlast (Maximum).

In Kraftwerken wird die Belastung mit geeigneten Erfassungsgeräten gemessen und aufgezeichnet. Diese Aufzeichnung wird dann mit einer Programm ausgewertet und liefert den verantwortlichen Kraftwerksbetreibern die gewünschten Ergebnisse.  Weiterhin sei angenommen, dass eine Rückfrage beim EVU (Energieversorgungsunternehmen) ergeben hat, dass alle 15 Minuten eine Leistungsmessung vorgenommen wird, also insgesamt 96 Messwerte für einen Tag zur Verfügung stehen. Für die Aufgabe wird davon ausgegangen, dass die Daten bereits im Array power gegeben sind und damit direkt im Programm verarbeitet werden können.

1. Das Grundgerüst des Programms wurde bereits erstellt - es umfasst das Einlesen der Messwerte, die Berechnung des Minimal- bzw. Maximalwerts sowie eine kurze Ausgabe der Ergebnisse. Allerdings haben sich Fehler eingeschlichen, die zu falschen Ergebnissen führen. Finden und korrigieren Sie die Fehler, so dass Minimal- und Maximalwert richtig berechnet werden.

2. Ergänzen Sie das Programm derart, dass nun auch die mittlere Leistung (arithmetischer Mittelwert) berechnet wird.

3. Geben Sie zusätzlich in der Ausgabe mit an, zu welcher Zeit die minimale und maximale Last am Tag auftraten.

4. Erweitern Sie Ihr Programm, so dass zusätzlich für jede einzelne Stunde (also 4 aufeinanderfolgende Messwerte) der Mittelwert berechnet und ausgegeben wird.

Achten Sie auch wieder auf eine gute Lesbarkeit ihres Programmcodes, damit andere es verstehen können.
